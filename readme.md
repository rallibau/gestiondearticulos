# GESTION-DE-AARTICULOS

Proyecto de pruebas
https://tanzu.vmware.com/developer/guides/ci-cd/argocd-gs/
kubectl port-forward svc/argocd-server -n argocd 8080:443

docker build . -t rallibau/gestiondearticulos:1.0.0
docker run -d -p 8085:9055 rallibau/gestiondearticulos:1.0.0 

docker login docker.io
docker push rallibau/gestiondearticulos:1.0.0


### Requerimientos de sistema
- Java Development Kit ³ ⁴ [Link](https://www.oracle.com/java/technologies/jdk8-downloads.html)
- Apache Maven 3.2.5


### Levantar aplicación
Para levantar la aplicación se tiene que lanzar el siguiente comando sobre la carpeta raiz:
> $ mvn spring-boot:run
Para lanzar los test
> $ mvn test:test


## Compilar en Maven
El proyecto puede ser gestionado por la herramienta Maven.
Para compilar el proyecto y probar que todas las depencias, así como la estabilidad de las clases, se puede lanzar el siguiente comando:
> $ mvn compile

Para compilar y empaquetar en un archivo JAR se lanza el siguiente comando:
> $ mvn clean package

### Carpeta target
Esta carpeta contiene los artefactos generados de la compilación y empaquetado de la aplicación por parte de Maven.

## Spring boot
Para iniciar spring fuera de un contenedor docker (no aconsejable), se utiliza el siguiente comando:
> $ mvn spring-boot:run

Acceder desde el navegador: http://localhost:9055/calculateTotalAmount?itemPrice=7500&itemDescription=tobacco&numberOfItems=2&state=VA


