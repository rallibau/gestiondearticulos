package es.rallibau.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.rallibau.dto.PedidoDto;
import es.rallibau.model.Descuento;
import es.rallibau.model.Producto;
import es.rallibau.model.Provincia;
import es.rallibau.repository.DescuentoRepository;
import es.rallibau.repository.ProductoRepository;
import es.rallibau.repository.ProvinciaRepository;

@Service
public class ImporteService {
	@Autowired
	ProvinciaRepository provinciaRepository;

	@Autowired
	ProductoRepository productoRepository;

	@Autowired
	DescuentoRepository descuentoRepository;

	/**
	 * Calcula el importe del pedido
	 * 
	 * @param pedido
	 * @return
	 */
	public BigDecimal calculateTotalAmount(PedidoDto pedido) {
		Integer itemsPedidos = Integer.valueOf(pedido.getNumberOfItems());
		Float importePorItem = Float.valueOf(pedido.getItemPrice());
		BigDecimal importeTotal = this.clacularImporteBase(itemsPedidos, importePorItem);
		return this.clacularImporteBase(itemsPedidos, importePorItem).subtract(calcularDescuento(importeTotal))
				.add(calcularImpuestoProvincia(importeTotal, pedido.getState()))
				.add(calcularImpuestoProducto(importeTotal, pedido.getItemDescription()));
	}

	/**
	 * nº items * importe
	 * 
	 * @param itemsPedidos
	 * @param importePorItem
	 * @return
	 */
	private BigDecimal clacularImporteBase(Integer itemsPedidos, Float importePorItem) {
		return BigDecimal.valueOf(itemsPedidos * importePorItem);
	}

	/**
	 * Busca el impuesto aplicable a la provincia
	 * 
	 * @param importeTotal
	 * @param codigoProvincia
	 * @return
	 */
	private BigDecimal calcularImpuestoProvincia(BigDecimal importeTotal, String codigoProvincia) {
		List<Provincia> provincias = provinciaRepository.findByCodigo(codigoProvincia);
		BigDecimal resultado = BigDecimal.valueOf(0);
		if (!provincias.isEmpty() && provincias.get(0).getImpuestoLocal() != null) {
			resultado = importeTotal.multiply(BigDecimal.valueOf(provincias.get(0).getImpuestoLocal() / 100));
		}
		return resultado;
	}

	/**
	 * Busca el impuesto aplicable al producto
	 * 
	 * @param importeTotal
	 * @param codigoProducto
	 * @return
	 */
	private BigDecimal calcularImpuestoProducto(BigDecimal importeTotal, String codigoProducto) {
		List<Producto> productos = productoRepository.findByCodigo(codigoProducto);
		BigDecimal resultado = BigDecimal.valueOf(0);
		if (!productos.isEmpty() && productos.get(0).getImpuestoEspecial() != null) {
			resultado = importeTotal.multiply(BigDecimal.valueOf(productos.get(0).getImpuestoEspecial() / 100));
		}
		return resultado;
	}

	/**
	 * Calcula el descuento aplicable
	 * 
	 * @param importeTotal
	 * @return
	 */
	private BigDecimal calcularDescuento(BigDecimal importeTotal) {
		BigDecimal resultado = BigDecimal.valueOf(0);
		List<Descuento> descuento = descuentoRepository.findDescuentoAplicable(importeTotal.doubleValue());
		if (descuento != null && !descuento.isEmpty() && descuento.get(0).getDescuentoAplicable() != null) {
			resultado = importeTotal.multiply(BigDecimal.valueOf(descuento.get(0).getDescuentoAplicable() / 100));
		}

		return resultado;
	}

}
