package es.rallibau.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DES_DESCUENTO")
public class Descuento {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "descuentoGenerator")
	@SequenceGenerator(name = "descuentoGenerator", sequenceName = "S_DESCUENTO")
	@Column(name = "DES_ID")
	private Long id;
	
	@Column(name = "DES_IMPORTE_UMBRAL")
	private Double importeUmbral;
	
	@Column(name = "DES_DESCUENTO_APLICABLE")
	private Double descuentoAplicable;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getImporteUmbral() {
		return importeUmbral;
	}

	public void setImporteUmbral(Double importeUmbral) {
		this.importeUmbral = importeUmbral;
	}

	public Double getDescuentoAplicable() {
		return descuentoAplicable;
	}

	public void setDescuentoAplicable(Double descuentoAplicable) {
		this.descuentoAplicable = descuentoAplicable;
	}
	
	

}
