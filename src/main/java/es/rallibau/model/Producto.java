package es.rallibau.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DD_PRD_PRODUCTO")
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "productoGenerator")
	@SequenceGenerator(name = "productoGenerator", sequenceName = "S_PRODUCTO")
	@Column(name = "DD_PRD_ID")
	private Long id;
	
	@Column(name = "DD_PRD_CODIGO")   
	private String codigo;
	 
	@Column(name = "DD_PRD_DESCRIPCION")   
	private String descripcion;
	
	@Column(name = "DD_PRD_IMPUESTO_ESPECIAL")
	private Double impuestoEspecial;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getImpuestoEspecial() {
		return impuestoEspecial;
	}

	public void setImpuestoEspecial(Double impuestoEspecial) {
		this.impuestoEspecial = impuestoEspecial;
	}
	
	
}
