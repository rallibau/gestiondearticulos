package es.rallibau.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ImporteTotalDto implements Serializable{
	private BigDecimal importeTotal;

	public BigDecimal getImporteTotal() {
		return importeTotal;
	}

	public void setImporteTotal(BigDecimal importeTotal) {
		this.importeTotal = importeTotal;
	}
	
}
