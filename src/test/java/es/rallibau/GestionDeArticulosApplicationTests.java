package es.rallibau;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class GestionDeArticulosApplicationTests {

	@Value("${server.port}")
	private int port;

	@Autowired
	private MockMvc mockMvc;


	@Test
	public void validandoParametrosObligatorios() throws Exception {
		/*
		 * this.mockMvc.perform(get("/calculateTotalAmount")).andDo(print()).andExpect(
		 * status().isBadRequest()) .andExpect(content().string(containsString("0")));
		 */

		mockMvc.perform(get("/calculateTotalAmount?itemDescription=sdfsdf&numberOfItems=2&state=VA")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());

		mockMvc.perform(
				get("/calculateTotalAmount?itemPrice=10&numberOfItems=2&state=VA").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());

		mockMvc.perform(get("/calculateTotalAmount?itemPrice=10&itemDescription=sdfsdf&state=VA")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());

		mockMvc.perform(get("/calculateTotalAmount?itemPrice=10&itemDescription=sdfsdf&numberOfItems=2")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());

		mockMvc.perform(get("/calculateTotalAmount?itemPrice=10&itemDescription=sdfsdf&numberOfItems=2&state=VA")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void validandoCasoUsoTomatto() throws Exception {
		mockMvc.perform(get("/calculateTotalAmount?itemPrice=10&itemDescription=tobacco&numberOfItems=2&state=VA")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.importeTotal").value(30.0));
	}

	@Test
	public void validandoCasoUsoProvinciaTeruel() throws Exception {
		mockMvc.perform(get("/calculateTotalAmount?itemPrice=10&itemDescription=random&numberOfItems=2&state=TER")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.importeTotal").value(20.1));
	}
	
	@Test
	public void validandoCasoUsoProvinciaCanarias() throws Exception {
		mockMvc.perform(get("/calculateTotalAmount?itemPrice=10&itemDescription=random&numberOfItems=2&state=CAN")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.importeTotal").value(20.94));
	}
	
	@Test
	public void validandoCasoUsoProvinciaCeutaYMelilla() throws Exception {
		mockMvc.perform(get("/calculateTotalAmount?itemPrice=10&itemDescription=random&numberOfItems=2&state=CYM")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.importeTotal").value(21.62));
	}

	@Test
	public void validandoCasoUsoProvinciaBaleares() throws Exception {
		mockMvc.perform(get("/calculateTotalAmount?itemPrice=10&itemDescription=random&numberOfItems=2&state=BAL")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.importeTotal").value(23.66));
	}

	@Test
	public void validandoCasoUsoRangoPrecios1000() throws Exception {
		// 1000
		mockMvc.perform(get("/calculateTotalAmount?itemPrice=500&itemDescription=random&numberOfItems=2&state=VA")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.importeTotal").value(970));
	}

	@Test
	public void validandoCasoUsoRangoPrecios2000() throws Exception {
		// 2000
		mockMvc.perform(get("/calculateTotalAmount?itemPrice=1000&itemDescription=random&numberOfItems=2&state=VA")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.importeTotal").value(1860));

	}

	@Test
	public void validandoCasoUsoRangoPrecios5000() throws Exception {

		// 5000
		mockMvc.perform(get("/calculateTotalAmount?itemPrice=2500&itemDescription=random&numberOfItems=2&state=VA")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.importeTotal").value(4250));
	}

	@Test
	public void validandoCasoUsoRangoPrecios7500() throws Exception {
		// 7500
		mockMvc.perform(get("/calculateTotalAmount?itemPrice=3750&itemDescription=random&numberOfItems=2&state=VA")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.importeTotal").value(6075));

	}

	@Test
	public void validandoCasoUsoRangoPrecios10000() throws Exception {
		// 10000
		mockMvc.perform(get("/calculateTotalAmount?itemPrice=5000&itemDescription=random&numberOfItems=2&state=VA")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.importeTotal").value(7500));
	}

}
