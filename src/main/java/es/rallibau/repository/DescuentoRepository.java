package es.rallibau.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import es.rallibau.model.Descuento;

@Repository
public interface DescuentoRepository extends JpaRepository<Descuento, Long> {

	@Query("SELECT des FROM Descuento des "
			+ "where des.importeUmbral <= :importe ORDER BY des.importeUmbral DESC")
	public List<Descuento> findDescuentoAplicable(@Param("importe") Double importe);
}
