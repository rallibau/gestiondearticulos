package es.rallibau;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionDeArticulosApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionDeArticulosApplication.class, args);
	}
	
	

}
