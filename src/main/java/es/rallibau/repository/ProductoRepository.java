package es.rallibau.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.rallibau.model.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long>{
	public List<Producto> findByCodigo(String codigo);
}
