package es.rallibau.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import es.rallibau.dto.ImporteTotalDto;
import es.rallibau.dto.PedidoDto;
import es.rallibau.repository.DescuentoRepository;
import es.rallibau.service.ImporteService;

@RestController
public class ProductoController {
	
	
	@Autowired
	ImporteService importeService;
	
	@Autowired
	DescuentoRepository descuentoRepository;
	
	
	@GetMapping(value = "/calculateTotalAmount")
	public @ResponseBody PedidoDto calculateTotalAmount(@Valid PedidoDto pedido) {
		pedido.setImporteTotal(importeService.calculateTotalAmount(pedido));
		return pedido;
	}

}
