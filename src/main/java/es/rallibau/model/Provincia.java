package es.rallibau.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DD_PRV_PROVINCIA")
public class Provincia {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "provinciaGenerator")
	@SequenceGenerator(name = "provinciaGenerator", sequenceName = "S_PROVINCIA")
	@Column(name = "DD_PRV_ID")
	private Long id;
	
	@Column(name = "DD_PRV_CODIGO")   
	private String codigo;
	 
	@Column(name = "DD_PRV_DESCRIPCION")   
	private String descripcion;
	
	@Column(name = "DD_PRV_IMPUESTO_LOCAL")
	private Double impuestoLocal;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getImpuestoLocal() {
		return impuestoLocal;
	}

	public void setImpuestoLocal(Double impuestoLocal) {
		this.impuestoLocal = impuestoLocal;
	}

	
	
	
	

}
