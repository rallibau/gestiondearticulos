package es.rallibau.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;

public class PedidoDto implements Serializable {
	@NotBlank(message = "itemDescription is mandatory")
	private String itemDescription;
	@NotBlank(message = "itemPrice is mandatory")
	private String itemPrice;
	@NotBlank(message = "numberOfItems is mandatory")
	private String numberOfItems;
	@NotBlank(message = "state is mandatory")
	private String state;
	private BigDecimal importeTotal;

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(String itemPrice) {
		this.itemPrice = itemPrice;
	}

	public String getNumberOfItems() {
		return numberOfItems;
	}

	public void setNumberOfItems(String numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public BigDecimal getImporteTotal() {
		return importeTotal;
	}

	public void setImporteTotal(BigDecimal importeTotal) {
		this.importeTotal = importeTotal;
	}

	
}
