package es.rallibau.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.rallibau.model.Provincia;

@Repository
public interface ProvinciaRepository extends JpaRepository<Provincia, Long>{
	public List<Provincia> findByCodigo(String codigo);
	
}
